package keeper

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
    "github.com/cosmos/cosmos-sdk/codec"
)

// CreatePlan creates a plan
func (k Keeper) CreatePlan(ctx sdk.Context, plan types.Plan) {
	store := ctx.KVStore(k.storeKey)
	key := []byte(types.PlanPrefix + plan.ID)
	value := k.cdc.MustMarshalBinaryLengthPrefixed(plan)
	store.Set(key, value)
}

// GetPlan returns the plan information
func (k Keeper) GetPlan(ctx sdk.Context, key string) (types.Plan, error) {
	store := ctx.KVStore(k.storeKey)
	var plan types.Plan
	byteKey := []byte(types.PlanPrefix + key)
	err := k.cdc.UnmarshalBinaryLengthPrefixed(store.Get(byteKey), &plan)
	if err != nil {
		return plan, err
	}
	return plan, nil
}

// SetPlan sets a plan
func (k Keeper) SetPlan(ctx sdk.Context, plan types.Plan) {
	planKey := plan.ID
	store := ctx.KVStore(k.storeKey)
	bz := k.cdc.MustMarshalBinaryLengthPrefixed(plan)
	key := []byte(types.PlanPrefix + planKey)
	store.Set(key, bz)
}

// DeletePlan deletes a plan
func (k Keeper) DeletePlan(ctx sdk.Context, key string) {
	store := ctx.KVStore(k.storeKey)
	store.Delete([]byte(types.PlanPrefix + key))
}

//
// Functions used by querier
//

func listPlan(ctx sdk.Context, k Keeper) ([]byte, error) {
	var planList []types.Plan
	store := ctx.KVStore(k.storeKey)
	iterator := sdk.KVStorePrefixIterator(store, []byte(types.PlanPrefix))
	for ; iterator.Valid(); iterator.Next() {
		var plan types.Plan
		k.cdc.MustUnmarshalBinaryLengthPrefixed(store.Get(iterator.Key()), &plan)
		planList = append(planList, plan)
	}
	res := codec.MustMarshalJSONIndent(k.cdc, planList)
	return res, nil
}

func getPlan(ctx sdk.Context, path []string, k Keeper) (res []byte, sdkError error) {
	key := path[0]
	plan, err := k.GetPlan(ctx, key)
	if err != nil {
		return nil, err
	}

	res, err = codec.MarshalJSONIndent(k.cdc, plan)
	if err != nil {
		return nil, sdkerrors.Wrap(sdkerrors.ErrJSONMarshal, err.Error())
	}

	return res, nil
}

// Get creator of the item
func (k Keeper) GetPlanOwner(ctx sdk.Context, key string) sdk.AccAddress {
	plan, err := k.GetPlan(ctx, key)
	if err != nil {
		return nil
	}
	return plan.Creator
}


// Check if the key exists in the store
func (k Keeper) PlanExists(ctx sdk.Context, key string) bool {
	store := ctx.KVStore(k.storeKey)
	return store.Has([]byte(types.PlanPrefix + key))
}
