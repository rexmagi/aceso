package AcesoHub

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/keeper"
)

func handleMsgCreatePlan(ctx sdk.Context, k keeper.Keeper, msg types.MsgCreatePlan) (*sdk.Result, error) {
	var plan = types.Plan{
		Creator: msg.Creator,
		ID:      msg.ID,
    	PlanName: msg.PlanName,
    	Description: msg.Description,
    	Premium: msg.Premium,
    	Deductible: msg.Deductible,
    	OutOfPocketMax: msg.OutOfPocketMax,
    	SupportingDocuments: msg.SupportingDocuments,
	}
	k.CreatePlan(ctx, plan)

	return &sdk.Result{Events: ctx.EventManager().Events()}, nil
}
