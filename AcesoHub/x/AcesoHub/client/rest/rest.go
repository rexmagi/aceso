package rest

import (
	"github.com/gorilla/mux"

	"github.com/cosmos/cosmos-sdk/client/context"
)

// RegisterRoutes registers AcesoHub-related REST handlers to a router
func RegisterRoutes(cliCtx context.CLIContext, r *mux.Router) {
  // this line is used by starport scaffolding # 1
		r.HandleFunc("/AcesoHub/plan", createPlanHandler(cliCtx)).Methods("POST")
		r.HandleFunc("/AcesoHub/plan", listPlanHandler(cliCtx, "AcesoHub")).Methods("GET")
		r.HandleFunc("/AcesoHub/plan/{key}", getPlanHandler(cliCtx, "AcesoHub")).Methods("GET")
		r.HandleFunc("/AcesoHub/plan", setPlanHandler(cliCtx)).Methods("PUT")
		r.HandleFunc("/AcesoHub/plan", deletePlanHandler(cliCtx)).Methods("DELETE")

		
}
