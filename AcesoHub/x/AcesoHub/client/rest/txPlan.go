package rest

import (
	"net/http"
	"strconv"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/types/rest"
	"github.com/cosmos/cosmos-sdk/x/auth/client/utils"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
)

// Used to not have an error if strconv is unused
var _ = strconv.Itoa(42)

type createPlanRequest struct {
	BaseReq rest.BaseReq `json:"base_req"`
	Creator string `json:"creator"`
	PlanName string `json:"planName"`
	Description string `json:"description"`
	Premium string `json:"premium"`
	Deductible string `json:"deductible"`
	OutOfPocketMax string `json:"outOfPocketMax"`
	SupportingDocuments string `json:"supportingDocuments"`
	
}

func createPlanHandler(cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req createPlanRequest
		if !rest.ReadRESTReq(w, r, cliCtx.Codec, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}
		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}
		creator, err := sdk.AccAddressFromBech32(req.Creator)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		
		parsedPlanName := req.PlanName
		
		parsedDescription := req.Description
		
		parsedPremium := req.Premium
		
		parsedDeductible := req.Deductible
		
		parsedOutOfPocketMax := req.OutOfPocketMax
		
		parsedSupportingDocuments := req.SupportingDocuments
		

		msg := types.NewMsgCreatePlan(
			creator,
			parsedPlanName,
			parsedDescription,
			parsedPremium,
			parsedDeductible,
			parsedOutOfPocketMax,
			parsedSupportingDocuments,
			
		)

		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		utils.WriteGenerateStdTxResponse(w, cliCtx, baseReq, []sdk.Msg{msg})
	}
}

type setPlanRequest struct {
	BaseReq rest.BaseReq `json:"base_req"`
	ID 		string `json:"id"`
	Creator string `json:"creator"`
	PlanName string `json:"planName"`
	Description string `json:"description"`
	Premium string `json:"premium"`
	Deductible string `json:"deductible"`
	OutOfPocketMax string `json:"outOfPocketMax"`
	SupportingDocuments string `json:"supportingDocuments"`
	
}

func setPlanHandler(cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req setPlanRequest
		if !rest.ReadRESTReq(w, r, cliCtx.Codec, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}
		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}
		creator, err := sdk.AccAddressFromBech32(req.Creator)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		
		parsedPlanName := req.PlanName
		
		parsedDescription := req.Description
		
		parsedPremium := req.Premium
		
		parsedDeductible := req.Deductible
		
		parsedOutOfPocketMax := req.OutOfPocketMax
		
		parsedSupportingDocuments := req.SupportingDocuments
		

		msg := types.NewMsgSetPlan(
			creator,
			req.ID,
			parsedPlanName,
			parsedDescription,
			parsedPremium,
			parsedDeductible,
			parsedOutOfPocketMax,
			parsedSupportingDocuments,
			
		)

		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		utils.WriteGenerateStdTxResponse(w, cliCtx, baseReq, []sdk.Msg{msg})
	}
}

type deletePlanRequest struct {
	BaseReq rest.BaseReq `json:"base_req"`
	Creator string `json:"creator"`
	ID 		string `json:"id"`
}

func deletePlanHandler(cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req deletePlanRequest
		if !rest.ReadRESTReq(w, r, cliCtx.Codec, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}
		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}
		creator, err := sdk.AccAddressFromBech32(req.Creator)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}
		msg := types.NewMsgDeletePlan(req.ID, creator)

		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		utils.WriteGenerateStdTxResponse(w, cliCtx, baseReq, []sdk.Msg{msg})
	}
}
