package cli

import (
	"bufio"
    
	"github.com/spf13/cobra"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/auth"
	"github.com/cosmos/cosmos-sdk/x/auth/client/utils"
	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
)

func GetCmdCreatePlan(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "create-plan [planName] [description] [premium] [deductible] [outOfPocketMax] [supportingDocuments]",
		Short: "Creates a new plan",
		Args:  cobra.ExactArgs(6),
		RunE: func(cmd *cobra.Command, args []string) error {
			argsPlanName := string(args[0] )
			argsDescription := string(args[1] )
			argsPremium := string(args[2] )
			argsDeductible := string(args[3] )
			argsOutOfPocketMax := string(args[4] )
			argsSupportingDocuments := string(args[5] )
			
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			inBuf := bufio.NewReader(cmd.InOrStdin())
			txBldr := auth.NewTxBuilderFromCLI(inBuf).WithTxEncoder(utils.GetTxEncoder(cdc))
			msg := types.NewMsgCreatePlan(cliCtx.GetFromAddress(), string(argsPlanName), string(argsDescription), string(argsPremium), string(argsDeductible), string(argsOutOfPocketMax), string(argsSupportingDocuments))
			err := msg.ValidateBasic()
			if err != nil {
				return err
			}
			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg})
		},
	}
}


func GetCmdSetPlan(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "set-plan [id]  [planName] [description] [premium] [deductible] [outOfPocketMax] [supportingDocuments]",
		Short: "Set a new plan",
		Args:  cobra.ExactArgs(7),
		RunE: func(cmd *cobra.Command, args []string) error {
			id := args[0]
			argsPlanName := string(args[1])
			argsDescription := string(args[2])
			argsPremium := string(args[3])
			argsDeductible := string(args[4])
			argsOutOfPocketMax := string(args[5])
			argsSupportingDocuments := string(args[6])
			
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			inBuf := bufio.NewReader(cmd.InOrStdin())
			txBldr := auth.NewTxBuilderFromCLI(inBuf).WithTxEncoder(utils.GetTxEncoder(cdc))
			msg := types.NewMsgSetPlan(cliCtx.GetFromAddress(), id, string(argsPlanName), string(argsDescription), string(argsPremium), string(argsDeductible), string(argsOutOfPocketMax), string(argsSupportingDocuments))
			err := msg.ValidateBasic()
			if err != nil {
				return err
			}
			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg})
		},
	}
}

func GetCmdDeletePlan(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "delete-plan [id]",
		Short: "Delete a new plan by ID",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {

			cliCtx := context.NewCLIContext().WithCodec(cdc)
			inBuf := bufio.NewReader(cmd.InOrStdin())
			txBldr := auth.NewTxBuilderFromCLI(inBuf).WithTxEncoder(utils.GetTxEncoder(cdc))

			msg := types.NewMsgDeletePlan(args[0], cliCtx.GetFromAddress())
			err := msg.ValidateBasic()
			if err != nil {
				return err
			}
			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg})
		},
	}
}
