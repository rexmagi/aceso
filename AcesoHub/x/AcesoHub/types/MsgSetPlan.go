package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgSetPlan{}

type MsgSetPlan struct {
  ID      string      `json:"id" yaml:"id"`
  Creator sdk.AccAddress `json:"creator" yaml:"creator"`
  PlanName string `json:"planName" yaml:"planName"`
  Description string `json:"description" yaml:"description"`
  Premium string `json:"premium" yaml:"premium"`
  Deductible string `json:"deductible" yaml:"deductible"`
  OutOfPocketMax string `json:"outOfPocketMax" yaml:"outOfPocketMax"`
  SupportingDocuments string `json:"supportingDocuments" yaml:"supportingDocuments"`
}

func NewMsgSetPlan(creator sdk.AccAddress, id string, planName string, description string, premium string, deductible string, outOfPocketMax string, supportingDocuments string) MsgSetPlan {
  return MsgSetPlan{
    ID: id,
		Creator: creator,
    PlanName: planName,
    Description: description,
    Premium: premium,
    Deductible: deductible,
    OutOfPocketMax: outOfPocketMax,
    SupportingDocuments: supportingDocuments,
	}
}

func (msg MsgSetPlan) Route() string {
  return RouterKey
}

func (msg MsgSetPlan) Type() string {
  return "SetPlan"
}

func (msg MsgSetPlan) GetSigners() []sdk.AccAddress {
  return []sdk.AccAddress{sdk.AccAddress(msg.Creator)}
}

func (msg MsgSetPlan) GetSignBytes() []byte {
  bz := ModuleCdc.MustMarshalJSON(msg)
  return sdk.MustSortJSON(bz)
}

func (msg MsgSetPlan) ValidateBasic() error {
  if msg.Creator.Empty() {
    return sdkerrors.Wrap(sdkerrors.ErrInvalidAddress, "creator can't be empty")
  }
  return nil
}