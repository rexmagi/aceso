package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
)

type Plan struct {
	Creator sdk.AccAddress `json:"creator" yaml:"creator"`
	ID      string         `json:"id" yaml:"id"`
    PlanName string `json:"planName" yaml:"planName"`
    Description string `json:"description" yaml:"description"`
    Premium string `json:"premium" yaml:"premium"`
    Deductible string `json:"deductible" yaml:"deductible"`
    OutOfPocketMax string `json:"outOfPocketMax" yaml:"outOfPocketMax"`
    SupportingDocuments string `json:"supportingDocuments" yaml:"supportingDocuments"`
}