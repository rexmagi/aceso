package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgDeletePlan{}

type MsgDeletePlan struct {
  ID      string         `json:"id" yaml:"id"`
  Creator sdk.AccAddress `json:"creator" yaml:"creator"`
}

func NewMsgDeletePlan(id string, creator sdk.AccAddress) MsgDeletePlan {
  return MsgDeletePlan{
    ID: id,
		Creator: creator,
	}
}

func (msg MsgDeletePlan) Route() string {
  return RouterKey
}

func (msg MsgDeletePlan) Type() string {
  return "DeletePlan"
}

func (msg MsgDeletePlan) GetSigners() []sdk.AccAddress {
  return []sdk.AccAddress{sdk.AccAddress(msg.Creator)}
}

func (msg MsgDeletePlan) GetSignBytes() []byte {
  bz := ModuleCdc.MustMarshalJSON(msg)
  return sdk.MustSortJSON(bz)
}

func (msg MsgDeletePlan) ValidateBasic() error {
  if msg.Creator.Empty() {
    return sdkerrors.Wrap(sdkerrors.ErrInvalidAddress, "creator can't be empty")
  }
  return nil
}