package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"
)

var _ sdk.Msg = &MsgCreatePlan{}

type MsgCreatePlan struct {
  ID      string
  Creator sdk.AccAddress `json:"creator" yaml:"creator"`
  PlanName string `json:"planName" yaml:"planName"`
  Description string `json:"description" yaml:"description"`
  Premium string `json:"premium" yaml:"premium"`
  Deductible string `json:"deductible" yaml:"deductible"`
  OutOfPocketMax string `json:"outOfPocketMax" yaml:"outOfPocketMax"`
  SupportingDocuments string `json:"supportingDocuments" yaml:"supportingDocuments"`
}

func NewMsgCreatePlan(creator sdk.AccAddress, planName string, description string, premium string, deductible string, outOfPocketMax string, supportingDocuments string) MsgCreatePlan {
  return MsgCreatePlan{
    ID: uuid.New().String(),
		Creator: creator,
    PlanName: planName,
    Description: description,
    Premium: premium,
    Deductible: deductible,
    OutOfPocketMax: outOfPocketMax,
    SupportingDocuments: supportingDocuments,
	}
}

func (msg MsgCreatePlan) Route() string {
  return RouterKey
}

func (msg MsgCreatePlan) Type() string {
  return "CreatePlan"
}

func (msg MsgCreatePlan) GetSigners() []sdk.AccAddress {
  return []sdk.AccAddress{sdk.AccAddress(msg.Creator)}
}

func (msg MsgCreatePlan) GetSignBytes() []byte {
  bz := ModuleCdc.MustMarshalJSON(msg)
  return sdk.MustSortJSON(bz)
}

func (msg MsgCreatePlan) ValidateBasic() error {
  if msg.Creator.Empty() {
    return sdkerrors.Wrap(sdkerrors.ErrInvalidAddress, "creator can't be empty")
  }
  return nil
}