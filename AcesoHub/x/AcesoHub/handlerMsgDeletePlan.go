package AcesoHub

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/keeper"
)

// Handle a message to delete name
func handleMsgDeletePlan(ctx sdk.Context, k keeper.Keeper, msg types.MsgDeletePlan) (*sdk.Result, error) {
	if !k.PlanExists(ctx, msg.ID) {
		// replace with ErrKeyNotFound for 0.39+
		return nil, sdkerrors.Wrap(sdkerrors.ErrInvalidRequest, msg.ID)
	}
	if !msg.Creator.Equals(k.GetPlanOwner(ctx, msg.ID)) {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "Incorrect Owner")
	}

	k.DeletePlan(ctx, msg.ID)
	return &sdk.Result{}, nil
}
