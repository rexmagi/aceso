package AcesoHub

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/types"
	"gitlab.com/rexmagi/AcesoHub/x/AcesoHub/keeper"
)

func handleMsgSetPlan(ctx sdk.Context, k keeper.Keeper, msg types.MsgSetPlan) (*sdk.Result, error) {
	var plan = types.Plan{
		Creator: msg.Creator,
		ID:      msg.ID,
    	PlanName: msg.PlanName,
    	Description: msg.Description,
    	Premium: msg.Premium,
    	Deductible: msg.Deductible,
    	OutOfPocketMax: msg.OutOfPocketMax,
    	SupportingDocuments: msg.SupportingDocuments,
	}
	if !msg.Creator.Equals(k.GetPlanOwner(ctx, msg.ID)) { // Checks if the the msg sender is the same as the current owner
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "Incorrect Owner") // If not, throw an error
	}

	k.SetPlan(ctx, plan)

	return &sdk.Result{Events: ctx.EventManager().Events()}, nil
}
