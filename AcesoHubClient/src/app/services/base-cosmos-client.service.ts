import { SigningCosmosClient } from '@cosmjs/launchpad';

export abstract class BaseCosmosClientService {
  public client: SigningCosmosClient;

  constructor() {}
}
