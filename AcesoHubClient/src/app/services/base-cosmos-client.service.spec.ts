import { TestBed } from '@angular/core/testing';

import { BaseCosmosClientService } from './base-cosmos-client.service';

describe('BaseCosmosClientService', () => {
  let service: BaseCosmosClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaseCosmosClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
