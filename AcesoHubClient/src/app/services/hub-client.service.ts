import { Injectable } from '@angular/core';
import { BaseCosmosClientService } from './base-cosmos-client.service';

@Injectable({
  providedIn: 'root',
})
export class HubClientService extends BaseCosmosClientService {
  constructor() {
    super();
  }
}
