import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetCoverageComponent } from './pages/get-coverage/get-coverage.component';
import { HomeComponent } from './pages/home/home.component';
import { PlansComponent } from './pages/plans/plans.component';
import { RootShellComponent } from './shells/root-shell/root-shell.component';

const routes: Routes = [
  {
    path: '',
    component: RootShellComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'get-coverage',
        component: GetCoverageComponent,
      },
      {
        path: 'plans',
        component: PlansComponent,
      },
    ],
  },
  {
    path: 'member',
    loadChildren: () =>
      import('./modules/member/member.module').then((m) => m.MemberModule),
  },
  {
    path: 'provider',
    loadChildren: () =>
      import('./modules/provider/provider.module').then(
        (m) => m.ProviderModule
      ),
  },
  {
    path: 'insurer',
    loadChildren: () =>
      import('./modules/insurer/insurer.module').then((m) => m.InsurerModule),
  },
  {
    path: 'governance',
    loadChildren: () =>
      import('./modules/governance/governance.module').then(
        (m) => m.GovernanceModule
      ),
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
