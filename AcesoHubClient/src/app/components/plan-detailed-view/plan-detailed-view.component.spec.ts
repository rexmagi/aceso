import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanDetailedViewComponent } from './plan-detailed-view.component';

describe('PlanDetailedViewComponent', () => {
  let component: PlanDetailedViewComponent;
  let fixture: ComponentFixture<PlanDetailedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanDetailedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanDetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
