import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanDetailedViewDialogComponent } from './plan-detailed-view-dialog.component';

describe('PlanDetailedViewDialogComponent', () => {
  let component: PlanDetailedViewDialogComponent;
  let fixture: ComponentFixture<PlanDetailedViewDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanDetailedViewDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanDetailedViewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
