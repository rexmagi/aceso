import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanSelectionListComponent } from './plan-selection-list.component';

describe('PlanSelectionListComponent', () => {
  let component: PlanSelectionListComponent;
  let fixture: ComponentFixture<PlanSelectionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanSelectionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanSelectionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
