export interface Plan {
  id: string;
  planName: string;
  premium: string;
  deductible: string;
  outOfPocketMax: string;
  supportingDocuments: Map<string, string>;
  creator: string;
}
