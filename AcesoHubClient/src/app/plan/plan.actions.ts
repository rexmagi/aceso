import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Plan } from './plan.model';

export const loadPlans = createAction(
  '[Plan/API] Load Plans', 
  props<{ plans: Plan[] }>()
);

export const addPlan = createAction(
  '[Plan/API] Add Plan',
  props<{ plan: Plan }>()
);

export const upsertPlan = createAction(
  '[Plan/API] Upsert Plan',
  props<{ plan: Plan }>()
);

export const addPlans = createAction(
  '[Plan/API] Add Plans',
  props<{ plans: Plan[] }>()
);

export const upsertPlans = createAction(
  '[Plan/API] Upsert Plans',
  props<{ plans: Plan[] }>()
);

export const updatePlan = createAction(
  '[Plan/API] Update Plan',
  props<{ plan: Update<Plan> }>()
);

export const updatePlans = createAction(
  '[Plan/API] Update Plans',
  props<{ plans: Update<Plan>[] }>()
);

export const deletePlan = createAction(
  '[Plan/API] Delete Plan',
  props<{ id: string }>()
);

export const deletePlans = createAction(
  '[Plan/API] Delete Plans',
  props<{ ids: string[] }>()
);

export const clearPlans = createAction(
  '[Plan/API] Clear Plans'
);
