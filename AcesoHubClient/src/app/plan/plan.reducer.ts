import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Plan } from './plan.model';
import * as PlanActions from './plan.actions';

export const plansFeatureKey = 'plans';

export interface State extends EntityState<Plan> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Plan> = createEntityAdapter<Plan>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});


export const reducer = createReducer(
  initialState,
  on(PlanActions.addPlan,
    (state, action) => adapter.addOne(action.plan, state)
  ),
  on(PlanActions.upsertPlan,
    (state, action) => adapter.upsertOne(action.plan, state)
  ),
  on(PlanActions.addPlans,
    (state, action) => adapter.addMany(action.plans, state)
  ),
  on(PlanActions.upsertPlans,
    (state, action) => adapter.upsertMany(action.plans, state)
  ),
  on(PlanActions.updatePlan,
    (state, action) => adapter.updateOne(action.plan, state)
  ),
  on(PlanActions.updatePlans,
    (state, action) => adapter.updateMany(action.plans, state)
  ),
  on(PlanActions.deletePlan,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(PlanActions.deletePlans,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(PlanActions.loadPlans,
    (state, action) => adapter.setAll(action.plans, state)
  ),
  on(PlanActions.clearPlans,
    state => adapter.removeAll(state)
  ),
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
