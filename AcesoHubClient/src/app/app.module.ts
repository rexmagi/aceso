import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import * as fromAuth from './reducers/auth.reducer';
import { MaterialModule } from './modules/material/material.module';
import { RootShellComponent } from './shells/root-shell/root-shell.component';
import { HomeComponent } from './pages/home/home.component';
import { GetCoverageComponent } from './pages/get-coverage/get-coverage.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlansComponent } from './pages/plans/plans.component';
import * as fromPlan from './plan/plan.reducer';
import { PlanEffects } from './plan/plan.effects';
import { AuthEffects } from './effects/auth.effects';
import * as fromClaim from './claim/claim.reducer';
import { ClaimEffects } from './claim/claim.effects';
import { PlanDetailedViewComponent } from './components/plan-detailed-view/plan-detailed-view.component';
import { PlanSelectionListComponent } from './components/plan-selection-list/plan-selection-list.component';
import { PlanListItemComponent } from './components/plan-selection-list/plan-list-item/plan-list-item.component';
import { PlanDetailedViewDialogComponent } from './components/plan-detailed-view-dialog/plan-detailed-view-dialog.component';
import { PlanListComponent } from './components/plan-list/plan-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RootShellComponent,
    HomeComponent,
    GetCoverageComponent,
    PlansComponent,
    PlanDetailedViewComponent,
    PlanSelectionListComponent,
    PlanListItemComponent,
    PlanDetailedViewDialogComponent,
    PlanListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forRoot(
      {
        [fromAuth.authFeatureKey]: fromAuth.reducer,
        [fromPlan.plansFeatureKey]: fromPlan.reducer,
        [fromClaim.claimsFeatureKey]: fromClaim.reducer,
      },
      {}
    ),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([PlanEffects, AuthEffects, ClaimEffects]),
    StoreRouterConnectingModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
