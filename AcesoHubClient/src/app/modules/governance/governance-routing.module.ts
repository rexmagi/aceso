import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GovernanceComponent } from './governance.component';

const routes: Routes = [{ path: '', component: GovernanceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GovernanceRoutingModule { }
