import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GovernanceRoutingModule } from './governance-routing.module';
import { GovernanceComponent } from './governance.component';


@NgModule({
  declarations: [GovernanceComponent],
  imports: [
    CommonModule,
    GovernanceRoutingModule
  ]
})
export class GovernanceModule { }
