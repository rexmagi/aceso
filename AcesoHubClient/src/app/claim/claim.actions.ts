import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Claim } from './claim.model';

export const loadClaims = createAction(
  '[Claim/API] Load Claims', 
  props<{ claims: Claim[] }>()
);

export const addClaim = createAction(
  '[Claim/API] Add Claim',
  props<{ claim: Claim }>()
);

export const upsertClaim = createAction(
  '[Claim/API] Upsert Claim',
  props<{ claim: Claim }>()
);

export const addClaims = createAction(
  '[Claim/API] Add Claims',
  props<{ claims: Claim[] }>()
);

export const upsertClaims = createAction(
  '[Claim/API] Upsert Claims',
  props<{ claims: Claim[] }>()
);

export const updateClaim = createAction(
  '[Claim/API] Update Claim',
  props<{ claim: Update<Claim> }>()
);

export const updateClaims = createAction(
  '[Claim/API] Update Claims',
  props<{ claims: Update<Claim>[] }>()
);

export const deleteClaim = createAction(
  '[Claim/API] Delete Claim',
  props<{ id: string }>()
);

export const deleteClaims = createAction(
  '[Claim/API] Delete Claims',
  props<{ ids: string[] }>()
);

export const clearClaims = createAction(
  '[Claim/API] Clear Claims'
);
