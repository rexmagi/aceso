import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ClaimEffects } from './claim.effects';

describe('ClaimEffects', () => {
  let actions$: Observable<any>;
  let effects: ClaimEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ClaimEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(ClaimEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
