import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Claim } from './claim.model';
import * as ClaimActions from './claim.actions';

export const claimsFeatureKey = 'claims';

export interface State extends EntityState<Claim> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Claim> = createEntityAdapter<Claim>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});


export const reducer = createReducer(
  initialState,
  on(ClaimActions.addClaim,
    (state, action) => adapter.addOne(action.claim, state)
  ),
  on(ClaimActions.upsertClaim,
    (state, action) => adapter.upsertOne(action.claim, state)
  ),
  on(ClaimActions.addClaims,
    (state, action) => adapter.addMany(action.claims, state)
  ),
  on(ClaimActions.upsertClaims,
    (state, action) => adapter.upsertMany(action.claims, state)
  ),
  on(ClaimActions.updateClaim,
    (state, action) => adapter.updateOne(action.claim, state)
  ),
  on(ClaimActions.updateClaims,
    (state, action) => adapter.updateMany(action.claims, state)
  ),
  on(ClaimActions.deleteClaim,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(ClaimActions.deleteClaims,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(ClaimActions.loadClaims,
    (state, action) => adapter.setAll(action.claims, state)
  ),
  on(ClaimActions.clearClaims,
    state => adapter.removeAll(state)
  ),
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
