import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-get-coverage',
  templateUrl: './get-coverage.component.html',
  styleUrls: ['./get-coverage.component.scss'],
})
export class GetCoverageComponent implements OnInit {
  getCoverageForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.getCoverageForm = fb.group({
      basicInfo: fb.group({
        firstName: ['', Validators.required],
        middleName: [''],
        lastName: ['', Validators.required],
        dateOfBirth: ['', Validators.required],
        wallet: ['', Validators.required],
        email: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
      }),
      plans: fb.array([]),
    });
  }

  ngOnInit(): void {}
}
