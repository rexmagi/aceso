import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCoverageComponent } from './get-coverage.component';

describe('GetCoverageComponent', () => {
  let component: GetCoverageComponent;
  let fixture: ComponentFixture<GetCoverageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetCoverageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCoverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
